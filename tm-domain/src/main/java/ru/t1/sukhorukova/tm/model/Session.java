package ru.t1.sukhorukova.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.dto.model.UserDTO;
import ru.t1.sukhorukova.tm.enumerated.Role;
import ru.t1.sukhorukova.tm.exception.entity.UserNotFoundException;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

@Setter
@Getter
@Entity
@NoArgsConstructor
@Table(name = "TM_SESSION")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class Session extends AbstractUserOwnerModel {

    @NotNull
    @Column(name = "LAST_DATE")
    private Date lastDate = new Date();

    @Nullable
    @Column(name = "ROLE")
    private Role role = null;

    public Session(@NotNull final User user) {
        if (user == null) throw new UserNotFoundException();
        this.setUser(user);
    }

}
