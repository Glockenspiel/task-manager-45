package ru.t1.sukhorukova.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.t1.sukhorukova.tm.api.repository.dto.ITaskDtoRepository;
import ru.t1.sukhorukova.tm.api.service.IConnectionService;
import ru.t1.sukhorukova.tm.api.service.dto.IProjectDtoService;
import ru.t1.sukhorukova.tm.api.service.dto.ITaskDtoService;
import ru.t1.sukhorukova.tm.enumerated.Status;
import ru.t1.sukhorukova.tm.enumerated.TaskSort;
import ru.t1.sukhorukova.tm.exception.entity.EntityNotFoundException;
import ru.t1.sukhorukova.tm.exception.entity.ProjectNotFoundException;
import ru.t1.sukhorukova.tm.exception.entity.TaskNotFoundException;
import ru.t1.sukhorukova.tm.exception.field.*;
import ru.t1.sukhorukova.tm.dto.model.TaskDTO;
import ru.t1.sukhorukova.tm.repository.dto.TaskDtoRepository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import java.util.List;

public final class TaskDtoService extends AbstractUserOwnedDtoService<TaskDTO, ITaskDtoRepository> implements ITaskDtoService {

    @NotNull
    final IProjectDtoService projectService;

    public TaskDtoService(
            @NotNull final IConnectionService connectionService,
            @NotNull final IProjectDtoService projectService
    ) {
        super(connectionService);
        this.projectService = projectService;
    }

    public ITaskDtoRepository getRepository(@NotNull final EntityManager entityManager) {
        return new TaskDtoRepository(entityManager);
    }

    @NotNull
    @Override
    public List<TaskDTO> findAll(
            @Nullable final String userId,
            @Nullable final TaskSort sort
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (sort == null) return findAll(userId);

        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskDtoRepository repository = getRepository(entityManager);
            return repository.findAll(userId, sort);
        } catch (@NotNull final NoResultException e) {
            return null;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public List<TaskDTO> findAllByProjectId(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();

        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskDtoRepository repository = getRepository(entityManager);
            return repository.findAllByProjectId(userId, projectId);
        } catch (@NotNull final NoResultException e) {
            return null;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public TaskDTO create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null) throw new DescriptionEmptyException();

        @NotNull final TaskDTO task = new TaskDTO();
        task.setName(name);
        task.setDescription(description);
        task.setStatus(Status.NOT_STARTED);

        return add(userId, task);
    }

    @NotNull
    private TaskDTO update(@Nullable final TaskDTO task) {
        if (task == null) throw new ProjectNotFoundException();

        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskDtoRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.update(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return task;
    }

    @NotNull
    @Override
    public TaskDTO updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new TaskIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null) throw new DescriptionEmptyException();

        @Nullable final TaskDTO task = findOneById(userId, id);
        if (task == null) throw new EntityNotFoundException();
        task.setName(name);
        task.setDescription(description);

        return update(task);
    }

    @NotNull
    @Override
    public TaskDTO updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= getSize()) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null) throw new DescriptionEmptyException();

        @Nullable final TaskDTO task = findOneByIndex(userId, index);
        if (task == null) throw new EntityNotFoundException();
        task.setName(name);
        task.setDescription(description);

        return update(task);
    }

    @NotNull
    @Override
    public TaskDTO changeTaskStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new TaskIdEmptyException();
        if (status == null) throw new StatusEmptyException();

        @Nullable final TaskDTO task = findOneById(userId, id);
        if (task == null) throw new EntityNotFoundException();
        task.setStatus(status);

        return update(task);
    }

    @NotNull
    @Override
    public TaskDTO changeTaskStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= getSize()) throw new IndexIncorrectException();
        if (index >= getSize()) throw new IndexIncorrectException();
        if (status == null) throw new StatusEmptyException();

        @Nullable final TaskDTO task = findOneByIndex(userId, index);
        if (task == null) throw new EntityNotFoundException();
        task.setStatus(status);

        return update(task);
    }

    @Override
    public void bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (!projectService.existsById(userId, projectId)) throw new ProjectNotFoundException();

        @Nullable final TaskDTO task = findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(projectId);

        update(task);
    }

    @Override
    public void unbindTaskFromProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (!projectService.existsById(userId, projectId)) throw new ProjectNotFoundException();

        @Nullable final TaskDTO task = findOneById(userId, taskId);
        System.out.println(task.getProjectId());
        System.out.println(projectId);
        if (task == null || !projectId.equals(task.getProjectId())) throw new TaskNotFoundException();
        task.setProjectId(null);

        update(task);
    }

    @Override
    public void removeProjectById(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (!projectService.existsById(userId, projectId)) throw new ProjectNotFoundException();

        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskDtoRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.removeTasksByProjectId(userId, projectId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }

        projectService.removeOneById(userId, projectId);
    }

}
